import csv
from db_queries import *
import collections
import matplotlib.pyplot as plt


def matches_won_of_all_teams_over_all_the_years_of_ipl():

    with open('matches.csv', 'rt') as csvfile1:
        match_file = csv.DictReader(csvfile1)
        matches_won_of_all_teams= collections.defaultdict(dict)

        for data in match_file:
            if data["season"] not  in matches_won_of_all_teams:
                if data["winner"] not in matches_won_of_all_teams[data['season']]:
                    matches_won_of_all_teams[data["season"]][data["winner"]] = 1
                else:
                    matches_won_of_all_teams[data["season"]][data["winner"]] += 1

            else:
                if data["winner"] not in matches_won_of_all_teams[data['season']]:
                    matches_won_of_all_teams[data["season"]][data["winner"]] = 1
                else:
                    matches_won_of_all_teams[data["season"]][data["winner"]] += 1


    return matches_won_of_all_teams



def Plot_stacked_bar_chart_of_matches_won_of_all_teams_over_all_the_years_of_ipl(data):
    season = sorted(data.keys())
    team_name = []

    for winner_team in data.values():
        team_name.extend(winner_team.keys())
    team_name = list(set(team_name))


    team_list = []
    ind_year = range(len(season))
    for team in team_name:
        team_wins = []
        for year in season:
            if team in data[year].keys():
                team_wins.append(data[year][team])
            else:
                team_wins.append(0)

        team_list.append({team: team_wins})

    flag_result = False
    positions = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for item in team_list:

        for key, val in item.items():
            if flag_result:
                bottom = list(map(lambda x, y: x + y, positions1, positions))
                positions = bottom
                plt.bar(ind_year, val, bottom=bottom, width=0.5)
            else:
                plt.bar(ind_year, val, width=0.5)

            flag_result = True
            positions1 = val

    plt.legend(team_name, loc="upper right", fontsize="x-small", bbox_to_anchor=(1.12, 1))
    plt.xlabel('Total Year')
    plt.ylabel('Number Of Won Match')
    plt.title('Winner Team per Year')
    plt.xticks(ind_year, season)
    plt.show()


def calculate_and_plot_the_number_of_matches_won_of_all_teams_over_all_the_years_of_ipl():
    result = matches_won_of_all_teams_over_all_the_years_of_ipl()
    result1 = matches_won_of_all_teams_over_all_the_years_of_ipl_sql()
    Plot_stacked_bar_chart_of_matches_won_of_all_teams_over_all_the_years_of_ipl(result1)


if __name__ == '__main__':
    calculate_and_plot_the_number_of_matches_won_of_all_teams_over_all_the_years_of_ipl()
