import csv
from db_queries import *
import matplotlib.pyplot as plt

def extra_runs_conceded_by_each_team_in_2016():
  with open('matches.csv', 'rt') as csvfile1, open('deliveries.csv', 'rt') as csvfile2:
    match_file = csv.DictReader(csvfile1)
    deliveries_file = csv.DictReader(csvfile2)
    match_ids = []
    extra_runs_conceded = {}
    for data in match_file:
      if '2016' in data["season"]:
        match_ids.append(data["id"])
    for data in deliveries_file:
      deliveries_match_id = data["match_id"]
      if deliveries_match_id in match_ids:

        if data["bowling_team"] not in extra_runs_conceded:
          extra_runs_conceded[data["bowling_team"]] = int(data["extra_runs"])
        else:
          extra_runs_conceded[data["bowling_team"]] += int(data["extra_runs"])

    return extra_runs_conceded


def plot_extra_runs_conceded_by_each_team_in_2016(data):

  team_name = list(data.keys())
  extra_runs= list(data.values())
  plt.bar(team_name,extra_runs , color ='g')
  plt.xlabel("Team names")
  plt.ylabel("Extra runs")
  plt.xticks(team_name, team_name , rotation=45)
  plt.title('extra runs conceded per team')
  plt.show()


def calculate_and_plot_extra_runs_conceded_by_each_team_in_2016():
  result = extra_runs_conceded_by_each_team_in_2016()
  result1 = extra_runs_conceded_by_each_team_in_2016_in_sql()
  plot_extra_runs_conceded_by_each_team_in_2016(result1)


if __name__ == '__main__':
  calculate_and_plot_extra_runs_conceded_by_each_team_in_2016()
