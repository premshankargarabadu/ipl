import csv
from db_queries import *
import matplotlib.pyplot as plt


def the_top_economical_bowlers_in_2015():
    with open('matches.csv', 'rt') as csvfile1, open('deliveries.csv', 'rt') as csvfile2:
        matches_file_reader = csv.DictReader(csvfile1)
        deliveries_file_reader = csv.DictReader(csvfile2)
        match_id = []
        total_no_of_balls = {}
        total_runs_dic = {}
        bowlers = {}
        economical_bowlers = {}
        for match in matches_file_reader:
            if '2015' in match["season"]:
                match_id.append(match["id"])
        for match in deliveries_file_reader:
            deliveries_match_id = match["match_id"]
            if deliveries_match_id in match_id:

                if match["bowler"] not in bowlers:
                    bowlers[match["bowler"]] = 1
                else:
                    bowlers[match["bowler"]] += 1

                if match["bowler"] not in total_runs_dic:
                    total_runs_dic[match["bowler"]] = int(match["total_runs"])
                else:
                    total_runs_dic[match["bowler"]] += int(match["total_runs"])
        total_no_of_balls = dict((v, (bowlers[v]) / 6.0) for v in bowlers)
        economical_bowlers = dict((v, (total_runs_dic[v]) / (total_no_of_balls[v])) for v in total_runs_dic)
        sorted_economical_bowlers_dic = dict(sorted(economical_bowlers.items(), key=lambda kv: kv[1]))

        return sorted_economical_bowlers_dic


def plot_the_top_economical_bowlers_in_2015(data):
    bowler_list = list(data.keys())
    eco_bowlers = list(data.values())

    plt.xlabel('bowler')
    plt.ylabel('economy')
    plt.title('top economical bowlers.')
    plt.bar(bowler_list[:5], eco_bowlers[:5], width=0.35, color='g')
    plt.show()




def calculate_and_plot_the_top_economical_bowlers_in_2015():
    result = the_top_economical_bowlers_in_2015()
    result1 = the_top_economical_bowlers_in_2015_in_sql()
    plot_the_top_economical_bowlers_in_2015(result1)




if __name__ == '__main__':
    calculate_and_plot_the_top_economical_bowlers_in_2015()
