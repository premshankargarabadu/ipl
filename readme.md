The project displays the five analysis:

    1.The number of matches played per year of all the years in IPL.
    2.Stacked bar chart of matches won of all teams over all the years of IPL.
    3.For the year 2016 plot the extra runs conceded per team.
    4.For the year 2015 plot the top economical bowlers.
    5.Calculating the maximum number of matches played in city.
