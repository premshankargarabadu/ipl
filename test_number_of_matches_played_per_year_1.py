import unittest

from number_of_matches_played_per_year_1 import *

class TestQ1(unittest.TestCase):
    def test_q1(self):
        expected_output = {'2017': 59, '2008': 58, '2009': 57, '2010': 60, '2011': 73, '2012': 74, '2013': 76, '2014': 60, '2015': 59, '2016': 60}
        output=  number_of_matches_played_per_year_of_all_the_years_in_ipl()
        self.assertEqual(output,expected_output)

if __name__ == '__main__':
    unittest.main()