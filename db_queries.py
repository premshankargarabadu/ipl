import psycopg2


def open_database():
  con = psycopg2.connect(database="postgres", user="postgres", password="prem@123", host="127.0.0.1", port="5432")
  print("Database opened successfully")
  cur = con.cursor()
  return cur,con



# con = psycopg2.connect(database="postgres", user="postgres", password="prem@123", host="127.0.0.1", port="5432")
# print("Database opened successfully")

# cur = con.cursor()
# cur.execute('''
#   CREATE TABLE matches(id integer NOT NULL,
#   season           varchar,
#   city             varchar,
#   date             date,
#   team1            varchar,
#   team2            varchar,
#   toss_winner      varchar,
#   toss_decision    varchar,
#   result           varchar,
#   dl_applied       integer,
#   winner           varchar,
#   win_by_runs      integer,
#   win_by_wickets   integer,
#   player_of_match  varchar,
#   venue            varchar,
#   umpire1          varchar,
#   umpire2          varchar,
#   umpire3          varchar);''')
# print("Match Table created successfully")

# cur.execute('''CREATE TABLE Deliveries
#    (match_id          SMALLINT,
#    inning             SMALLINT,
#    batting_team       TEXT,
#    bowling_team       TEXT,
#    over               SMALLINT,
#    ball               SMALLINT,
#    batsman            TEXT,
#    non_striker        TEXT,
#    bowler             TEXT,
#    is_super_over      SMALLINT,
#    wide_runs          SMALLINT,
#    bye_runs           SMALLINT,
#    legbye_runs        SMALLINT,
#    noball_runs        SMALLINT,
#    penalty_runs       SMALLINT,
#    batsman_runs       SMALLINT,
#    extra_runs         SMALLINT,
#    total_runs         SMALLINT,
#    player_dismissed   TEXT,
#    dismissal_kind     TEXT,
#    fielder            TEXT
#    );''')

# print("Deliveries Table created successfully")
# cur.execute("COPY matches FROM '/home/prem/PycharmProjects/IPL/matches.csv' DELIMITER ',' CSV HEADER;")
# cur.execute("COPY Deliveries FROM '/home/prem/PycharmProjects/IPL/deliveries.csv' DELIMITER ',' CSV HEADER;")

# con.commit()
# con.close()


def  number_of_matches_played_per_year_of_all_the_years_in_ipl_in_sql():
  cur,con=open_database()
  cur.execute("SELECT season ,count(season)FROM matches GROUP BY season ORDER BY season")
  rows = cur.fetchall()
  print("Operation done successfully")
  con.commit()
  con.close()
  return dict(rows)


def matches_won_of_all_teams_over_all_the_years_of_ipl_sql():
  cur,con=open_database()
  cur.execute("SELECT season,winner,count(winner) from matches   group by winner,season order by season")
  No_of_count_of_winner=cur.fetchall()
  con.commit()
  print("Record inserted successfully")
  con.close()
  count_of_winner= {}
  for  data in No_of_count_of_winner:

      year = data[0]
      team = data[1]
      wins = data[2]
      if  year in count_of_winner:
          if team in count_of_winner[year]:
              count_of_winner[year][team] += wins
          else:
              count_of_winner[year][team] = wins
      else:
          count_of_winner[year] = {team:wins}
  # print(count_of_winner)
  return count_of_winner




def extra_runs_conceded_by_each_team_in_2016_in_sql():
  cur,con=open_database()
  cur.execute("SELECT  bowling_team ,sum(extra_runs) as sum_of_extra_runs from Deliveries  inner join matches on matches.id = Deliveries.match_id  where season = '2015' GROUP BY bowling_team")
  rows = cur.fetchall()
  print("Operation done successfully")
  con.commit()
  con.close()
  return dict(rows)


def the_top_economical_bowlers_in_2015_in_sql():
  cur,con=open_database()
  cur.execute("SELECT deliveries.bowler, (SUM(deliveries.total_runs)/(COUNT(deliveries.bowler)/6.0)) as run_rate from Deliveries INNER JOIN matches ON Deliveries.match_id=matches.id and matches.season='2015' GROUP BY bowler ORDER BY run_rate")

  rows = cur.fetchall()
  print("Operation done successfully")
  con.commit()
  con.close()
  # print(dict(rows))
  return dict(rows)



def  number_of_matches_played_per_city_of_all_the_years_in_ipl_in_sql():
  cur,con=open_database()
  cur.execute("SELECT city ,count(city)FROM matches GROUP BY city ORDER BY city")
  rows = cur.fetchall()
  print("Operation done successfully")
  con.commit()
  con.close()
  return dict(rows)



