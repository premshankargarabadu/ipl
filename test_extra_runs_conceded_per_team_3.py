import unittest

from extra_runs_conceded_per_team_3 import *

class TestQ3(unittest.TestCase):
    def test_q3(self):
        expected_output = {'Rising Pune Supergiants': 108, 'Mumbai Indians': 102, 'Kolkata Knight Riders': 122, 'Delhi Daredevils': 106, 'Gujarat Lions': 98, 'Kings XI Punjab': 100, 'Sunrisers Hyderabad': 107, 'Royal Challengers Bangalore': 156}
        output= extra_runs_conceded_by_each_team_in_2016()
        self.assertEqual(output,expected_output)
        # print(output)

if __name__ == '__main__':
    unittest.main()