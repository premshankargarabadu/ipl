import unittest

from number_of_matches_played_per_year_of_all_the_years_in_ipl_5 import *

class TestQ5(unittest.TestCase):
    def test_q5(self):
        expected_output = {'Hyderabad': 49, 'Pune': 32, 'Rajkot': 10, 'Indore': 5, 'Bangalore': 66, 'Mumbai': 85, 'Kolkata': 61, 'Delhi': 60, 'Chandigarh': 46, 'Kanpur': 4, 'Jaipur': 33, 'Chennai': 48, 'Cape Town': 7, 'Port Elizabeth': 7, 'Durban': 15, 'Centurion': 12, 'East London': 3, 'Johannesburg': 8, 'Kimberley': 3, 'Bloemfontein': 2, 'Ahmedabad': 12, 'Cuttack': 7, 'Nagpur': 3, 'Dharamsala': 9, 'Kochi': 5, 'Visakhapatnam': 11, 'Raipur': 6, 'Ranchi': 7, 'Abu Dhabi': 7, 'Sharjah': 6, '': 7}
        output=  number_of_matches_played_per_city_of_all_the_years_in_ipl()
        self.assertEqual(output,expected_output)

if __name__ == '__main__':
    unittest.main()