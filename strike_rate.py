import csv


def get_top_batsman_by_strike_rate_for_each_year(matches_file_path, deliveries_file_path, year):
  id_match = {}
  with open(matches_file_path, 'r') as csv_file_matches:
    csv_reader = csv.DictReader(csv_file_matches)
    for match in csv_reader:
      if match['season'] == str(year):
        if match['id'] not in id_match:
          id_match[match['id']] = 0
  batsman_data = {}
  count = {}
  with open(deliveries_file_path, 'r') as csv_file_deliveries:
    csv_reader = csv.DictReader(csv_file_deliveries)
    for delivery in csv_reader:
      if delivery['match_id'] in id_match:
        if delivery['batsman'] not in batsman_data:
          batsman_data[delivery['batsman']] = int(delivery['batsman_runs'])
          count[delivery['batsman']] = 1
        else:
          batsman_data[delivery['batsman']] += int(delivery['batsman_runs'])
          count[delivery['batsman']] += 1
  for key in batsman_data:
    batsman_data[key] = batsman_data[key] / count[key]
  return batsman_data


def get_top_batsman_by_strike_rate_for_all_years(matches_file_path, deliveries_file_path):
  top_batsman_by_strike_rate_for_all_years = {}
  for year in range(2008, 2018):
    match = get_top_batsman_by_strike_rate_for_each_year(matches_file_path, deliveries_file_path, year)
    match_in_list = dict(sorted(match.items(), key=lambda kv: kv[1], reverse=True)[:5])

  for year in range(2008, 2018):
    top_batsman_by_strike_rate_for_all_years[year] = match_in_list
  print(top_batsman_by_strike_rate_for_all_years)
  # for key , value in top_batsman_by_strike_rate_for_all_years.items():
  # print(key, " : ", value)
  return top_batsman_by_strike_rate_for_all_years


if __name__ == '__main__':
  get_top_batsman_by_strike_rate_for_all_years('./matches.csv', './deliveries.csv')
