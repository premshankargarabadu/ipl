import csv
from db_queries import *
import matplotlib.pyplot as plt

def  number_of_matches_played_per_year_of_all_the_years_in_ipl():


    ipl_file_reader = csv.DictReader(open("matches.csv"))
    matches_played_per_year_container = {}
    for data in ipl_file_reader:
        if data["season"] not in matches_played_per_year_container.keys():
            matches_played_per_year_container[data["season"]]=1
        else:
            matches_played_per_year_container[data["season"]]+=1

    return matches_played_per_year_container

def plot_the_number_of_matches_played_per_year_of_all_the_years_in_ipl(data):


    plt.xlabel('season')
    plt.ylabel('number of matches')
    plt.title('number of matches played per year of all the years in IPL.')
    plt.bar(list(data.keys()), list(data.values()))
    plt.show()


def calculate_and_plot_the_number_of_matches_played_per_year_of_all_the_years_in_ipl():

    result = number_of_matches_played_per_year_of_all_the_years_in_ipl()
    result1=number_of_matches_played_per_year_of_all_the_years_in_ipl_in_sql()
    plot_the_number_of_matches_played_per_year_of_all_the_years_in_ipl(result1)


if __name__ == '__main__':

    calculate_and_plot_the_number_of_matches_played_per_year_of_all_the_years_in_ipl()
