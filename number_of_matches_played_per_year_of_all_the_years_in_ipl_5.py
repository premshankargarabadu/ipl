import csv
import psycopg2
from db_queries import *
import matplotlib.pyplot as plt


def  number_of_matches_played_per_city_of_all_the_years_in_ipl():
    ipl_file_reader = csv.DictReader(open("matches.csv"))
    matches_played_per_city_container = {}
    for data in ipl_file_reader:
        if data["city"] not in matches_played_per_city_container.keys():
            matches_played_per_city_container[data["city"]]=1
        else:
            matches_played_per_city_container[data["city"]]+=1

    return matches_played_per_city_container


def plot_the_number_of_matches_played_per_city_of_all_the_years_in_ipl(data):
    city_name = list(data.keys())
    number_of_matches= list(data.values())
    plt.bar(city_name,number_of_matches)
    plt.xlabel("city")
    plt.ylabel("number of matches")
    plt.xticks(city_name , rotation=90)
    plt.title('number of matches played per city of all the years in IPL.')
    plt.show()

def calculate_and_plot_the_number_of_matches_played_per_city_of_all_the_years_in_ipl():
    result = number_of_matches_played_per_city_of_all_the_years_in_ipl()
    result1= number_of_matches_played_per_city_of_all_the_years_in_ipl_in_sql()
    plot_the_number_of_matches_played_per_city_of_all_the_years_in_ipl(result1)


if __name__ == '__main__':
    calculate_and_plot_the_number_of_matches_played_per_city_of_all_the_years_in_ipl()
